import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss'],
})
export class OrganizationComponent implements OnInit {

  public event;
  // public description: string;

  constructor(
    public router: Router,
  ) { }

  ngOnInit(): void {
  }

}
