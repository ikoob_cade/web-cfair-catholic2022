import * as _ from 'lodash';
import { _ParseAST } from '@angular/compiler';
import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { EventService } from '../../services/api/event.service';
import { Router } from '@angular/router';
import { AlertService } from '../../services/alert.service';
import { SliderControllerService } from '../../services/function/sliderController.service';
import { BannerService } from '../../services/api/banner.service';
import { NgImageSliderComponent } from 'ng-image-slider';

declare var $: any;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit, AfterViewInit {
  @ViewChild('entryAlertBtn') entryAlert: ElementRef;
  @ViewChild('bannerSlider') bannerSlider: NgImageSliderComponent;
  public speakers: Array<any> = [];
  public sponsors: Array<any> = []; // 스폰서 목록
  public selectedBooth: any;
  public mobile: boolean;

  public programBookUrl = '';
  public excerptUrl = '';

  public user;
  public attachments = []; // 부스 첨부파일
  public boothCategories: Array<any> = []; // 카테고리[부스] 목록
  public event;

  public speakerLoaded = false;
  public speakerDetail;
  public banners = [];

  constructor(
    private eventService: EventService,
    private router: Router,
    private alertService: AlertService,
    private bannerService: BannerService,
    private sliderControllerService: SliderControllerService
  ) {}

  ngOnInit(): void {
    this.checkEventVersion();
  }

  // 버전확인
  checkEventVersion(): void {
    this.eventService.findOne().subscribe((res) => {
      this.event = res;
      if (this.event.clientVersion) {
        const clientVersion = localStorage.getItem(
          this.event.eventCode + 'ver'
        );
        if (!clientVersion) {
          localStorage.setItem(
            this.event.eventCode + 'ver',
            this.event.clientVersion
          );
          location.reload();
        } else if (clientVersion) {
          if (clientVersion !== this.event.clientVersion) {
            localStorage.setItem(
              this.event.eventCode + 'ver',
              this.event.clientVersion
            );
            location.reload();
          }
        }
      }

      this.user = sessionStorage.getItem('cfair');
      if (!this.user) {
        this.router.navigate(['/login']);
      }
    });
  }

  ngAfterViewInit(): void {
    // * 메인팝업 */
    // this.alertService.mainPopupWithCookie(this.entryAlert);

    this.getBanners();
  }

  // ! Banner Slider Controller
  /** 배너 목록 조회 */
  getBanners(): void {
    this.bannerService.find().subscribe((res) => {
      if (_.keys(res).length > 0) {
        res.live.forEach((item) => {
          const data = {
            link: item.link,
            thumbImage: item.photoUrl,
            alt: item.title,
          };
          this.banners.push(data);
        });
      }
    });
  }
  /** 광고 구좌 왼쪽 버튼 클릭 */
  slidePrev(target): void {
    this.sliderControllerService.slidePrev(this, target);
  }
  /** 광고 구좌 오른쪽 버튼 클릭 */
  slideNext(target): void {
    this.sliderControllerService.slideNext(this, target);
  }
  /** 광고 배너 클릭 */
  imageClick(index): void {
    this.sliderControllerService.imageClick(this.banners, index);
  }
}
