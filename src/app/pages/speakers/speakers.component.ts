import * as _ from 'lodash';
import { Component, OnInit, HostListener } from '@angular/core';
import { CategoryService } from '../../services/api/category.service';
import { SpeakerService } from '../../services/api/speaker.service';
import { Router } from '@angular/router';

declare let $: any;
@Component({
  selector: 'app-speakers',
  templateUrl: './speakers.component.html',
  styleUrls: ['./speakers.component.scss']
})
export class SpeakersComponent implements OnInit {

  public speakers: Array<any> = [];
  public mobile: boolean = false;

  public speakerCategories: Array<any> = [];

  public speakerDetail;
  public speakerLoaded = false;

  public user = sessionStorage.getItem('cfair');

  constructor(
    private speakerService: SpeakerService,
    private categoryService: CategoryService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.mobile = window.innerWidth < 768;
    // this.loadCategories();
    this.loadSpeakers();
  }


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.mobile = window.innerWidth < 768;
  }


  // 발표자 리스트를 조회한다.
  loadSpeakers = () => {
    this.speakerService.find(false).subscribe(res => {
      this.speakers = res;
      this.loadCategories();
    });
  }

  loadCategories = () => {
    this.categoryService.findByCategoryIds('speaker').subscribe(res => {
      _.forEach(res, category => {
        category.speakers = _.sortBy(category.speakers, 'seq');
      });
      this.speakerCategories = res;
    });
  }

  getDetail(speaker): void {
    setTimeout(() => {
      this.speakerLoaded = false;
    }, 0);

    if (this.user) {
      this.speakerService.findOne(speaker.id).subscribe(res => {
        console.log('==>', res);
        this.speakerDetail = res;
        this.speakerLoaded = true;
        $('#speakersModal').modal('show');
      });
    } else {
      this.router.navigate(['/login']);
    }
  }
}
