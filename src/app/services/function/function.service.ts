import { Injectable } from "@angular/core";

@Injectable()
export class FunctionService {
    constructor() {
    }

    scrollToTop(): void {
        window.scrollTo({ top: 0 });
    }

    /**
     * 배열 나누기
     * @param array 배열
     * @param n n개씩 자르기
     */
    division(array, n): any {
        return [].concat.apply([],
            array.map((item, i) => {
                return i % n ? [] : [array.slice(i, i + n)];
            })
        );
    }
}
