export const environment = {
  production: true,
  base_url: 'https://cfair-api.cf-air.com/api/v1', // 운영서버
  socket_url: 'https://cfair-socket-ssl.cf-air.com',
  eventId: '61de6f0e8e8ca6001270301a' // catholic 2022 Prod
};
