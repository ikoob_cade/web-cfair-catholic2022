export const environment = {
  production: true,
  base_url: 'https://d3v-server-cf-air-api.ikoob.co.kr/api/v1',
  socket_url: 'https://d3v-server-cf-air-socket-api.ikoob.co.kr',
  eventId: '61de6fc81b8f6a1487f79cd7' // catholic 2022 Dev
};
